package view;

import java.awt.GridLayout;
import java.util.List;
import java.util.Map;

import javax.swing.JFrame;
import javax.swing.JPanel;

public class View {
	private JFrame mainFrame;
	private GraphPanel graphPanel;
	private JPanel mainPanel;
	
	public View(List<Double> points) {
		buildGraph(points);
	}

	private void buildGraph(List<Double> points) {
		this.mainFrame = new JFrame("Exemplo Gráfico java");
		this.mainFrame.setSize(800, 600);
        this.mainFrame.setLocation(293, 70);

		mainPanel = new JPanel(new GridLayout(1, 1));
		graphPanel = new GraphPanel(points);
		mainPanel.add(graphPanel);
		mainFrame.add(mainPanel);
		mainFrame.setVisible(true);
	}

}
